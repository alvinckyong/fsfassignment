/**
 * Created by alvinyong on 11/7/16.
 */

var express = require("express");
var path = require("path");

var app = express();
const DEFAULT_PORT_NUMBER = 3000;

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/bower_components'));

var bodyParser = require("body-parser"); // Body parser for fetch posted data
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); // Body parser to use JSON data

app.post("/save", function(req, res) { 
    console.log("Saving on server side");
    console.log(req.body.params.user);
    var stringify = JSON.stringify(req.body.params.user);
    var user = JSON.parse(stringify);
    console.info(">>> user: %s", user);
    res.status(202).end();
});

app.use(function(req, res, next) {
    res.type("text/plain");
    res.status(404);
    res.end("Page not found");
});

console.info("Argument passed in as PORT %s", process.argv[2]);
app.set("port", process.argv[2] || process.env.APP_PORT || DEFAULT_PORT_NUMBER);

app.listen(app.get("port") , function() {
    console.info("Middleware started on port %s", app.get("port"));
});

